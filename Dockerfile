FROM debian:jessie

RUN \
 apt-get update -qy && \
 apt-get install -qy bzip2 python build-essential

ADD oecore-x86_64-core2-64-toolchain-nodistro.0.sh entrypoint.sh /

RUN \
 /oecore-x86_64-core2-64-toolchain-nodistro.0.sh -y -d /opt/gc-sdk && \
 rm /oecore-x86_64-core2-64-toolchain-nodistro.0.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/bin/bash"]
