#!/bin/bash

set -e

. /opt/gc-sdk/environment-setup-core2-64-oe-linux

exec "$@"
